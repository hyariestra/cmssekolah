<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hubungi extends CI_Controller {
	public function index(){
		if (isset($_POST['submit'])){
			if ($this->input->post() && (strtolower($this->input->post('secutity_code')) == strtolower($this->session->userdata('mycaptcha')))) {
				$result = $this->model_utama->kirim_Pesan();

				if ($result) {
				
				echo $this->session->set_flashdata('message', '<div class="alert alert-info">Pesan anda berhasil terkirim, dan akan segera kami respon...!</div>');
				}
				redirect('Hubungi');
			}else{
				echo $this->session->set_flashdata('message', '<div class="alert alert-danger">Pesan gagal terkirim,Cek Kembali Captcha Anda</div>');
				redirect('Hubungi');
			}
		}else{
			$data['title'] = 'Hubungi Kami';
			$this->load->helper('captcha');
			$vals = array(
	            'img_path'	 => './captcha/',
	            'img_url'	 => base_url().'captcha/',
	            'font_path' => 'asset/Tahoma.ttf',
	            'font_size'     => 20,
	            'img_width'	 => '150',
	            'img_height' => 35,
	            'border' => 0, 
	            'word_length'   => 3,
	            'expiration' => 7200
	        );

	        $cap = create_captcha($vals);
	        $data['image'] = $cap['image'];
	        $this->session->set_userdata('mycaptcha', $cap['word']);
			$this->template->load(template().'/template',template().'/view_hubungi',$data);
		}
	}
}
