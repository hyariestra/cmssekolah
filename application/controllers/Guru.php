<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Guru extends CI_Controller {
	public function index(){
		$data['title'] = 'Guru dan Staff';
		$jumlah= $this->model_utama->hitungguru()->num_rows();
		$config['base_url'] = base_url().'guru/index';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 20; 	
			if ($this->uri->segment('3')!=''){
				$dari = $this->uri->segment('3');
			}else{
				$dari = 0;
			}

			if (is_numeric($dari)) {
				$data['guru'] = $this->model_utama->guru($dari, $config['per_page']);
			}else{
				redirect('guru');
			}
		$this->pagination->initialize($config);
		$this->template->load(template().'/template',template().'/view_guru',$data);
	}


	public function profil($value='')
	{
		$ids = decrypt_url($this->uri->segment(3));

		$data['record'] = $this->model_utama->guru_detail($ids)->row_array();
		$data['title'] ='Profil '.$data['record']['nama'];

		$this->template->load(template().'/template',template().'/view_guru_detail',$data);
	}

	public function detail(){
		$ids = $this->uri->segment(3);
		$dat = $this->db->query("SELECT * FROM agenda a LEFT JOIN users b ON a.username = b.id where tema_seo='$ids' OR id_agenda='$ids'");
	    $row = $dat->row();
	    $total = $dat->num_rows();
	        if ($total == 0){
	        	redirect('utama');
	        }
		$data['title'] = $row->tema;
		$data['record'] = $this->model_utama->agenda_detail($ids)->row_array();
		$this->template->load(template().'/template',template().'/view_agenda_detail',$data);
	}
}
