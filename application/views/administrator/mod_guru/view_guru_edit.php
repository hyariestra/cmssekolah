<?php 
echo "<div class='col-md-12'>
<div class='box box-info'>
<div class='box-header with-border'>
<h3 class='box-title'>Ubah Data Guru</h3>
</div>
<div class='box-body'>";
$attributes = array('class'=>'form-horizontal','role'=>'form');
echo form_open_multipart('administrator/save_guru',$attributes); 
echo "<div class='col-md-12'>
<table class='table table-condensed table-bordered'>
<tbody>
<input type='hidden' name='id' value='$id'>
<tr><th width='120px' scope='row'>Email</th><td><input value='$detail[username]' type='email' class='form-control' name='email' required>
<span>*Email akan digunakan untuk login ke website</span></td></tr>
<tr><th width='120px' scope='row'>Password</th><td><input type='password' class='form-control' name='password'>
<input type='hidden' value='$detail[password]' class='form-control' name='old_password'>
<span>*biarkan kosong jika tidak ingin dirubah<span>
</td></tr>
<tr><th width='120px' scope='row'>NIP</th><td><input value='$detail[nip]'  type='text' class='form-control' name='nip'></td></tr>
<tr><th width='120px' scope='row'>Nama</th><td><input value='$detail[nama]' type='text' class='form-control' name='nama' required></td></tr>

<tr>
<th scope='row'>Status</th>
<td>
<select onchange='mySelectHandler()' required name='guruStaff' class='form-control' >
<option value='' selected>- Pilih Status -</option>";

foreach ($status->result_array() as $row){
  $selected = '';
  if($detail['id_status'] == $row['id_status']){
    $selected = ' selected="" ';
  }
  echo "<option value='$row[id_status]'".$selected.">$row[nama_status]</option>";
}
echo "</td>
</tr>


<tr id='mengajar'><th scope='row'>Mengajar</th><td>
<select name='mapel' class='form-control'>
<option value='' selected>- Pilih Mapel -</option>";
foreach ($mapel->result_array() as $row){
  $selected = '';
  if($detail['mapel_id'] == $row['id']){
    $selected = ' selected="" ';
  }
  echo "<option value='$row[id]'".$selected.">$row[mapel]</option>";
}
echo "</td></tr>
<tr>
<th width='100px' scope='row'>Tanggal Lahir</th>
<td><input value='$tgl'  autocomplete=off readonly id='singlerangepicker' type='text' class='form-control' name='tanggalLahir' required>
</td>
</tr>
<tr><th scope='row'>Tempat Lahir</th>
<td><textarea class='form-control' name='tempat_lahir' style='height:120px'>$detail[tempat_lahir]
</textarea>
</td>
</tr>
<tr>
<th scope='row'>Jenis Kelamin</th>
<td>
<select name='jenis_kelamin' class='form-control' required>
<option value='' selected>- Pilih Jenis Kelamin -</option>";
echo '<option value="Pria" '.(($detail['jenis_kelamin']=='Pria')?'selected="selected"':"").'>Pria</option>';
echo '<option value="Wanita" '.(($detail['jenis_kelamin']=='Wanita')?'selected="selected"':"").'>Wanita</option>';
echo "</td>
</tr>
<tr><th scope='row'>Agama</th><td><select name='agama' class='form-control' required>
<option value='' selected>- Pilih Agama -</option>";
foreach ($agama->result_array() as $row){
 $selected = '';
 if($detail['agama_id'] == $row['id']){
  $selected = ' selected="" ';
}
echo "<option value='$row[id]'".$selected.">$row[agama]</option>";
}
echo "</td></tr>

<tr><th scope='row'>Alamat Tinggal</th>
<td><textarea  class='form-control' name='alamat' style='height:100px' >$detail[alamat]
</textarea>
</td>
</tr>
<tr><th scope='row'>Hobi</th>
<td><textarea style='width:100%' class='textarea'  class='form-control' name='hobi' style='height:100px'>$detail[hobi]
</textarea>
</td>
</tr>
<tr><th scope='row'>Riwayat Pendidkan</th>
<td><textarea style='width:100%' class='textarea' class='form-control' name='pendidikan' >$detail[pendidikan]
</textarea>
</td>
</tr>";
echo "<tr><th width='120px' scope='row'>Telpon</th><td><input value='$detail[telp]' type='text' class='form-control' name='telp' required></td></tr>";


if ($this->session->all_userdata()['level']=='admin') {

  echo "<th scope='row'>Status</th>
  <td>
  <select name='status' class='form-control' required>
  <option value='' selected>- Pilih Status -</option>";
  echo '<option value="Y" '.(($detail['blokir']=='Y')?'selected="selected"':"").'>Aktif</option>';
  echo '<option value="N" '.(($detail['blokir']=='N')?'selected="selected"':"").'>Tidak Aktif</option>';
  echo "</td>
  </tr>

  <th scope='row'>Level</th>
  <td>
  <select name='level' class='form-control' required>
  <option value='' selected>- Pilih Level -</option>";
  echo '<option value="user" '.(($detail['level']=='user')?'selected="selected"':"").'>User</option>';
  echo '<option value="admin" '.(($detail['level']=='admin')?'selected="selected"':"").'>Admin</option>';
  echo "</td>
  </tr>";


  echo "<tr>
  <th scope='row'>Tampilkan Halaman Guru dan Staff </th>
  <td>
  <select name='show' class='form-control' required>
  <option value='' selected>- Pilih -</option>";
  echo '<option value="Y" '.(($detail['show']=='Y')?'selected="selected"':"").'>Y</option>';
  echo '<option value="N" '.(($detail['show']=='N')?'selected="selected"':"").'>N</option>';
  echo "</td>
  </tr>";

}else{

 echo "<td><input  type='hidden' class='form-control' value='$detail[level]' name='level_old'>
 <input  type='hidden' class='form-control' value='$detail[show]' name='show_old'>
 <input  type='hidden' class='form-control' value='$detail[blokir]' name='status_old'></td>";
}




echo "<tr><th scope='row'>Foto</th>
<td>
<img style='margin-bottom:10px;' width='200px' src='".base_url()."asset/foto_guru/$detail[foto_enc]' alt='$detail[foto]'>
<input accept='.JPG,.PNG,.JPEG'  type='file' class='form-control' name='foto'>
<input  type='hidden' class='form-control' value='$detail[foto]' name='foto_old'>
<input  type='hidden' class='form-control' value='$detail[foto_enc]' name='foto_enc_old'>
<span>*File harus berformat JPG,.PNG,.JPEG dan ukuran maksimal 2MB</span>
</td>
</tr>


</tbody>
</table>
</div>
</div>
<div class='box-footer'>
<input type='submit' name='submit' value='Ubah' class='btn btn-info'>
<a href='".base_url().$this->uri->segment(1)."/manajemenuser'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>

</div>
</div>";
?>

<script>

  $( document ).ready(function() {
   mySelectHandler();
 });


  function mySelectHandler() {
    var mySelect  = $('select[name="guruStaff"]').val();

    if (mySelect == 3) {
      $("#mengajar").show();
    }else{
      $("#mengajar").removeAttr("style").hide();
    }

  }

  $('.form-horizontal').ajaxForm({ 
    dataType:  'json', 
    beforeSubmit: function(formData, jqForm, options){


    },
    success:   processJson,
    error: processJsonError
  });


  function processJsonError(result) {
    result = result.responseJSON;
    processJson(result, true);
  }

  function processJson(result) { 

    new Noty({
      text: result.message,
      type: result.status_code,
      timeout: 3000,
      theme: 'semanticui'
    }).show();

    if(result.status == 201){
     window.location = '<?php echo $url_view?>';

   }
 }

</script>