<?php 
echo "<div class='col-md-12'>
<div class='box box-info'>
<div class='box-header with-border'>
<h3 class='box-title'>Tambah Guru Baru</h3>
</div>
<div class='box-body'>";
$attributes = array('class'=>'form-horizontal','role'=>'form');
echo form_open_multipart('administrator/save_guru',$attributes); 
echo "<div class='col-md-12'>
<table class='table table-condensed table-bordered'>
<tbody>
<input type='hidden' name='id' value=''>
<tr><th width='120px' scope='row'>Email</th><td><input type='email' class='form-control' name='email' required>
<span>*Email akan digunakan untuk login ke website</span></td></tr>
<tr><th width='120px' scope='row'>Password</th><td><input type='password' class='form-control' name='password' required></td></tr>
<tr><th width='120px' scope='row'>NIP</th><td><input  type='text' class='form-control' name='nip' ></td></tr>
<tr><th width='120px' scope='row'>Nama</th><td><input  type='text' class='form-control' name='nama' ></td></tr>

<tr>
<th scope='row'>Status</th>
<td>
<select onchange='mySelectHandler()' required name='guruStaff' class='form-control' >
<option value='' selected>- Pilih Status -</option>";
foreach ($status->result_array() as $row){
  echo "<option value='$row[id_status]'".@$selected.">$row[nama_status]</option>";
}
echo "</td>
</tr>

<tr style=display:none id='mengajar'><th scope='row'>Mengajar</th><td><select name='mapel' class='form-control'>";
echo "<option value=''>- Pilih Mata Pelajaran -</option>";
foreach ($mapel->result_array() as $row){
  echo "<option value='$row[id]'".$selected.">$row[mapel]</option>";
}
echo "</td></tr>
<tr>
<th width='100px' scope='row'>Tanggal Lahir</th>
<td><input autocomplete=off readonly id='singlerangepicker' type='text' class='form-control' name='tanggalLahir' >
</td>
</tr>
<tr><th scope='row'>Tempat Lahir</th>
<td><textarea  class='form-control' name='tempat_lahir' style='height:120px' >
</textarea>
</td>
</tr>
<tr>
<th scope='row'>Jenis Kelamin</th>
<td>
<select name='jenis_kelamin' class='form-control' >
<option value='' selected>- Pilih Jenis Kelamin -</option>";
echo "<option value='Pria'>Pria</option>";
echo "<option value='Wanita'>Wanita</option>";
echo "</td>
</tr>
<tr><th scope='row'>Agama</th><td><select name='agama' class='form-control' >";
foreach ($agama->result_array() as $row){
  echo "<option value='$row[id]'>$row[agama]</option>";
}
echo "</td></tr>

<tr><th scope='row'>Alamat Tinggal</th>
<td><textarea  class='form-control' name='alamat' style='height:100px' >
</textarea>
</td>
</tr>
<tr><th scope='row'>Hobi</th>
<td><textarea style='width:100%' class='textarea'  class='form-control' name='hobi' style='height:100px' >
</textarea>
</td>
</tr>
<tr><th scope='row'>Riwayat Pendidkan</th>
<td><textarea style='width:100%' class='textarea' class='form-control' name='pendidikan' >
</textarea>
</td>
</tr>

<tr><th width='120px' scope='row'>Telpon</th><td><input type='text' class='form-control' name='telp' ></td></tr>
<th scope='row'>Aktif</th>
<td>
<select name='status' class='form-control' required>
<option value='' selected>- Pilih Status -</option>";
echo "<option value='Y'>Aktif</option>";
echo "<option value='N'>Tidak Aktif</option>";
echo "</td>
</tr>

<th scope='row'>Level</th>
<td>
<select name='level' class='form-control' required>
<option value='' selected>- Pilih Level -</option>";
echo "<option value='user'>User</option>";
echo "<option value='admin'>Admin</option>";
echo "</td>
</tr>

<tr>
<th scope='row'>Tampilkan Halaman Guru dan Staff </th>
<td>
<select name='show' class='form-control' required>
<option value='' selected>- Pilih -</option>";
echo "<option value='Y'>Ya</option>";
echo "<option value='N'>Tidak</option>";
echo "</td>
</tr>

<tr><th scope='row'>Foto</th>
<td><input accept='.JPG,.PNG,.JPEG'  type='file' class='form-control' name='foto'>
<span>*File harus berformat JPG,.PNG,.JPEG dan ukuran maksimal 2MB</span>
</td>
</tr>


</tbody>
</table>
</div>
</div>
<div class='box-footer'>
<input type='submit' name='submit' value='Tambah' class='btn btn-info'>
<a href='".base_url().$this->uri->segment(1)."/dataguru'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>

</div>
</div>";
?>

<script>

  function mySelectHandler() {
  var mySelect  = $('select[name="guruStaff"]').val();

  if (mySelect == 3) {
    $("#mengajar").show();
  }else{
    $("#mengajar").removeAttr("style").hide();
  }

}

$('.form-horizontal').ajaxForm({ 
  dataType:  'json', 
  beforeSubmit: function(formData, jqForm, options){


  },
  success:   processJson,
  error: processJsonError
});


function processJsonError(result) {
  result = result.responseJSON;
  processJson(result, true);
}

function processJson(result) { 

  new Noty({
    text: result.message,
    type: result.status_code,
    timeout: 3000,
    theme: 'semanticui'
  }).show();

  if(result.status == 201){
   window.location = '<?php echo $url_view?>';

 }
}

</script>