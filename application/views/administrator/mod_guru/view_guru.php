            <div class="col-xs-12">  
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Semua User</h3>
                  <?php if ($this->session->all_userdata()['level']=='admin'): ?>
                    
                  <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url(); ?>administrator/tambah_guru'>Tambahkan User</a>
                  <?php endif ?>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                  <?php       echo "<center>".$this->session->flashdata('message')."</center>"; ?>
                    <thead>
                      <tr>
                        <th style='width:20px'>No</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Telpon</th>
                        <th>Status</th>
                        <th style='width:50px'>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                  <?php 
                    $no = 1;
                    foreach ($record->result_array() as $row){
                    if ($row['status_guru']=='Y'){
                      $kategori = '<span class="label label-primary">Aktif</span>';
                    }else{
                      $kategori = '<span class="label label-danger">Tidak Aktif</span>';
                    }
                    echo "<tr><td>$no</td>
                              <td>$row[nip]</td>
                              <td>$row[nama]</td>
                              <td>$row[username]</td>
                              <td>$row[telp]</td>
                              <td>$kategori</td>
                              <td><center>
                                <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url()."administrator/ubah_guru/".encrypt_url($row['id_guru'])."'><span class='glyphicon glyphicon-edit'></span></a>";

                                if ($this->session->all_userdata()['level']=='admin') {
                            
                                echo "<a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url()."administrator/delete_guru/".encrypt_url($row['id_guru'])."' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>";
                                }
                             
                             echo  "</center>
                              </td>
                          </tr>";
                      $no++;
                    }
                  ?>
                  </tbody>
                </table>
              </div>