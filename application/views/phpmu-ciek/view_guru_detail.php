<?php
echo "<p class='sidebar-title'> $title</p>";

if ($record['foto_enc'] == ''){ $foto = 'user.png'; }else{ $foto = $record['foto_enc']; }


if ($record['jabatan']==1) {
  $status = 'Guru';
}elseif ($record['jabatan']==2) {
  $status = 'Karyawan / Staff';
}else{
  $status = '-';
}


?>

<div class="block table-responsive">
  <table class="table table-bordered" style="font-size:0.9em!important">
    <tbody><tr>
      <td style="width: 110px;padding:5px" rowspan="7">
        <img src="<?php echo base_url()?>asset/foto_guru/<?php echo $foto ?> "  class="rounded" width="200px" height="200px" alt="<?php echo $record['nama'] ?>" title="<?php echo $record['nama'] ?>">

      </td>    
    </tr>
    <tr>
      <td style="width: 132px;">Name</td>
      <td><?php echo $record['nama'];  ?></td>
    </tr>
    
    <tr>
      <td>Status</td>
      <td><?php echo $record['nama_status'];  ?></td>
    </tr>
    <tr>
      <td>Mengajar</td>
      <td><?php echo $record['mapel'];  ?></td>
    </tr>
    <tr>
      <td>Telepon</td>
      <td><?php echo $record['telp'];  ?></td>
    </tr>
    <tr>
      <td>Email</td>
      <td><?php echo $record['username'];  ?></td>
    </tr>



  </tbody></table>
</div>
<div id="list_detail">

  <div class="block" id="tabs">
    <div class="block-content tab-content" id="tab_content">

      <div id="data_biodata" class="tab-pane active" role="tabpanel">
        <h5>Personal Information</h5>
        <div class="table-responsive">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th style="width: 30%;">Tempat Lahir</th>
                <td><?php echo $record['tempat_lahir'] ?></td>
              </tr>
              <tr>
                <th>Tanggal Lahir</th>
                <td><?php echo tgl_indoo($record['tanggal_lahir']) ?></td>
              </tr>
              <tr>
                <th>Alamat</th>
                <td><?php echo $record['alamat'] ?></td>
              </tr>
              <tr>
                <th>Gender</th>
                <td><?php echo $record['jenis_kelamin'] ?></td>
              </tr>
              <tr>
                <th>Agama</th>
                <td><?php echo $record['agama'] ?></td>
              </tr>
              <tr>
                <th>Hobi</th>
                <td><?php echo $record['hobi'] ?></td>
              </tr>
              <tr>
                <th>Pendidikan</th>
                <td><?php echo $record['pendidikan'] ?></td>
              </tr>
              


            </tbody>
          </table>
        </div>

    
              </div>

            </div>


          </div>


        </div>
        