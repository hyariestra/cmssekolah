<!DOCTYPE html>
<html lang="en">
  <head>
   <title><?php include "phpmu-title.php"; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="description" content="<?php include "phpmu-description.php"; ?>">
    <meta name="keywords" content="<?php include "phpmu-keywords.php"; ?>">
    <meta name="author" content="phpmu.com">
    <meta http-equiv="imagetoolbar" content="no">
    <meta name="language" content="Indonesia">
    <meta name="revisit-after" content="7">
    <meta name="webcrawlers" content="all">
    <meta name="rating" content="general">
    <meta name="spiders" content="all">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>asset/<?php echo getSetting('favicon') ?>"/>
    <link href="<?php echo base_url(); ?>template/<?php echo template(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>template/<?php echo template(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>template/<?php echo template(); ?>/css/red.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>template/<?php echo template(); ?>/css/default.css" rel="stylesheet" media="screen" />
    <link href="<?php echo base_url(); ?>template/<?php echo template(); ?>/css/nivo-slider.css" rel="stylesheet" media="screen" />
  </head>

  <body>
    <div class="container container-content">
    <?php echo "<small class='pull-right waktu'>".hari_ini().", ".tgl_indoo(date('Y-m-d')).", <span id='jam'></span> WIB</small>"; ?>
      <img class="img-responsive" style='padding:20px' src='<?php echo base_url(); ?>asset/logo.png'>
      <nav class="navbar navbar-default">
        <?php include "main-menu.php"; ?>
      </nav>

      <div class="breaking-news">
          <span class="the-title">File Terbaru</span>
          <ul>
              <?php
                $terkini = $this->db->query("SELECT * FROM download ORDER BY id_download DESC LIMIT 0,10 ");
                foreach ($terkini->result_array() as $row) {
                  echo "<li><a href='".base_url()."download'>$row[judul]</a></li>";
                }
              ?>
          </ul>
      </div>

      <br>
      <div class='row'>
        <div class='col-md-12'>
          <div class='col-md-8'>
              <?php 
                if ($this->uri->segment(1)=='' OR $this->uri->segment(1)=='utama'){
                  include "slide.php"; 
                }
              ?>
              <?php echo $contents; ?>
              <br>
          </div>
          <div class='col-md-4'>
              <?php include "sidebar.php"; ?>
          </div>
        </div>
      </div>

   <section style="margin-top: 20px;" class="index-link">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="link-area">
                  <img alt="logo sd bhayangkara" style="padding: 10px" width="200px" height="200px" src="<?php echo base_url() ?>asset/foto_info/logo-sdn-bhayangkara.png " alt="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="link-area">
                    <h3>Media Sosial</h3>
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fweb.facebook.com%2Fsdnbhayangkara%2F&tabs&width=340&height=200&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId"  width="265" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                </div>
            </div>
            <div class="col-md-3">
                <div class="link-area">
                    <h3>Navigasi</h3>
                    <ul>
                    <li><a href="<?php echo base_url() ?>"> Beranda</a></li>
                    <li><a href="<?php echo base_url() ?>agenda "> Agenda</a></li>
                    <li><a href="<?php echo base_url() ?>berita"> Berita</a></li>
                    <li><a href="<?php echo base_url() ?>download"> Download</a></li>
                    <li><a href="<?php echo base_url() ?>gallery"> Galeri</a></li>
                    <li><a href="<?php echo base_url() ?>hubungi"> Hubungi Kami</a></li>
                    <li><a href="<?php echo base_url() ?>guru"> Guru Dan Staff</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="link-area">
                    <h3>HUBUNGI KAMI</h3>
                    <ul>
                    <li><a href="#"> <?php echo getSetting('telp') ?></a></li>
                    <li><a href="#"> <?php echo getSetting('email') ?></a></li>
                    <li><a href="#"> <?php echo getSetting('alamat') ?></a></li>
                    <li><a target="_blank" href="<?php echo getSetting('facebook') ?>">Facebook</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="index-social">
    <div class="container">
    <div class="row index-social-link text-center">
            <p class="copy-c">Copyright © 2020 - <?php echo date("Y"); ?>  <a class="foot" href="sdnbhayangkara.sch.id">sdnbhayangkara.sch.id</a>  All rights reserved. </p>
        </div>
        </div>
</section>
    </div> <!-- /container -->
    <?php $this->model_utama->kunjungan(); ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>template/<?php echo template(); ?>/js/jquery-1.12.3.min.js"></script>
    <script src="<?php echo base_url(); ?>template/<?php echo template(); ?>/js/bootstrap.min.js"></script>
    

    <script src="<?php echo base_url(); ?>template/<?php echo template(); ?>/js/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>template/<?php echo template(); ?>/js/phpmu-custom.js"></script>
    <script> $(document).ready(function(){ $("#formku").validate(); }); $('#myLightbox').lightbox(options);</script>

</body>
</html>
