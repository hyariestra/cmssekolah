<?php 
class Model_guru extends CI_model{

   private $_table = "users";

   function __construct()
   {

    parent::__construct();

    $this->id_sess = $this->session->userdata('id');
    $this->level = $this->session->userdata('level');

}


function guru(){

  $this->db->select('*,guru.blokir AS status_guru,guru.id as id_guru');
  $this->db->from('users guru');
  $this->db->join('mapel','mapel.id=guru.mapel','left');

  if ($this->level!='admin') {
    $this->db->where('guru.id',$this->id_sess);
}

$this->db->order_by('nama','desc');
return $query = $this->db->get();

}

function list_guru_tambah($datadb){

   $result = true;    
   $this->db->trans_begin();


   $this->db->insert('users',$datadb);


   if ($this->db->trans_status() === FALSE){
    $this->db->trans_rollback();
    $result = false;
}else{
    $this->db->trans_commit();
    $result = true;
}

return $result;

}


function list_guru_edit($datadb){
    $result = true;    
    $this->db->trans_begin();

    $this->db->where('id',$datadb['id']);
    $this->db->update('users',$datadb);

    if ($this->db->trans_status() === FALSE){
        $this->db->trans_rollback();
        $result = false;
    }else{
        $this->db->trans_commit();
        $result = true;
    }

    return $result;

}

function guru_delete($id)
{
    $result = true;    
    $this->db->trans_begin();

    $foto = $this->db->query("SELECT foto_enc FROM users WHERE id = '$id' ")->row_array();

    

    if (isset($foto)) {
        
        $path_to_file = FCPATH.'asset/foto_guru/'.$foto['foto_enc'];
        unlink($path_to_file); 

    }
    

    $this->db->query("DELETE FROM users where id='$id'");

    if ($this->db->trans_status() === FALSE){
        $this->db->trans_rollback();
        $result = false;
    }else{
        $this->db->trans_commit();
        $result = true;
    }

    return $result;
}



function get_email_user($username,$id='')
{

    return $this->db->query("SELECT username FROM users WHERE username = '".$username."' AND id NOT IN ('".$id."') ")->num_rows();

}

function get_nip_user($nip,$id='')
{

    return $this->db->query("SELECT nip FROM users WHERE nip = '".$nip."' AND id NOT IN ('".$id."') ")->num_rows();

}


function get_detail_guru($id){
    return $this->db->query("SELECT *,a.mapel AS mapel_id,a.`agama` AS agama_id, a.`status` AS status FROM users a
        LEFT JOIN mapel b ON a.`mapel` = b.`id`
        LEFT JOIN agama c ON a.`agama` = c.`id`
        LEFT JOIN `status` d ON d.`id_status` = a.`status`
        WHERE a.`id` = ".$id." ")->row_array();
}



function agenda_delete($id){
    return $this->db->query("DELETE FROM agenda where id_agenda='$id'");
}
}